FROM node:9.7.0-alpine
MAINTAINER miguel@myrotarycar.com

USER root

WORKDIR /root

COPY bin ./bin/
COPY public ./public/
COPY routes ./routes/
COPY views ./views/
COPY app.js ./app.js
COPY package.json ./package.json

RUN npm install

# EXPOSE 3000
ENTRYPOINT ["/root/bin/start"]
